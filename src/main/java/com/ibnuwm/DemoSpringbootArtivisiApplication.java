package com.ibnuwm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringbootArtivisiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoSpringbootArtivisiApplication.class, args);
	}

}
