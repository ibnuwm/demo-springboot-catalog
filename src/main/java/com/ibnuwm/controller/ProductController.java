package com.ibnuwm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ibnuwm.dao.ProductDao;
import com.ibnuwm.entity.Product;

@RestController
@RequestMapping("/api/product")
public class ProductController {
	
	@Autowired
	private ProductDao productDao;
	
	@GetMapping
	public Page<Product> findProducts(Pageable pageable) {
		return productDao.findAll(pageable);
		
	}
	
	@GetMapping("/{id}")
	public Product findById(@PathVariable("id") Product p){
		return p;
	}

}
