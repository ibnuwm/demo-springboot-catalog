package com.ibnuwm.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ibnuwm.entity.Product;

public interface ProductDao extends PagingAndSortingRepository<Product, String> {

}
